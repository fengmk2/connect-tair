/*!
 * connect-tair - test/connect-tair.test.js
 * Copyright(c) 2013 fengmk2 <fengmk2@gmail.com> (http://fengmk2.github.com)
 * MIT Licensed
 */

"use strict";

/**
 * Module dependencies.
 */

var connect = require('connect');
var RedisStore = require('../')(connect);
var should = require('should');
var Tair = require('tair');
var mm = require('mm');

describe('connect-tair.test.js', function () {
  var tair = new Tair({
    dataId: 'market2-daily',
    diamond: 'commonconfig.taobao.net:8080',
    namespace: 171
  });

  var store = new RedisStore({
    client: tair
  });

  before(function (done) {
    store.client.on('connect', done);
  });

  afterEach(mm.restore);

  after(function () {
    tair.emit('error', new Error('end error'));
    tair.close();
  });

  describe('set()', function () {
    it('should set session ok', function (done) {
      store.set('123', { cookie: { maxAge: 2000 }, name: 'mk' }, function (err, ok) {
        should.not.exist(err);
        should.ok(ok);
        done();
      });
    });

    it('should set circular data error', function (done) {
      var obj = {};
      obj.a = {b:obj};
      store.set('123circular', obj, function (err, ok) {
        should.exist(err);
        err.name.should.equal('TypeError');
        err.message.should.equal('Converting circular structure to JSON');
        should.ok(!ok);
        done();
      });
    });
  });

  describe('get()', function () {
    it('should get session ok', function (done) {
      store.get('123', function (err, data) {
        should.not.exist(err);
        data.should.eql({ cookie: { maxAge: 2000 }, name: 'mk' });
        done();
      });
    });

    it('should return mock error', function (done) {
      mm.error(tair, 'get');
      store.get('123', function (err, data) {
        should.exist(err);
        should.ok(!data);
        done();
      });
    });

    it('should return mock JSON.parse error', function (done) {
      mm.data(tair, 'get', '{');
      store.get('123', function (err, data) {
        should.exist(err);
        err.name.should.equal('SyntaxError');
        err.message.should.equal('Unexpected end of input');
        should.ok(!data);
        done();
      });
    });
  });

  describe('destroy()', function () {
    it('should destroy session ok', function (done) {
      store.destroy('123', function (err) {
        should.not.exist(err);
        store.get('123', function (err, data) {
          should.not.exist(err);
          should.not.exist(data);
          done();
        });
      });
    });
  });
});
