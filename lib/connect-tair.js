/*!
 * connect-tair - lib/connect-tair.js
 * Copyright(c) 2013 fengmk2 <fengmk2@gmail.com> (http://fengmk2.github.com)
 * MIT Licensed
 */

"use strict";

/**
 * Module dependencies.
 */

var debug = require('debug')('connect-tair');
var util = require('util');

/**
 * One day in seconds.
 */

var oneDay = 86400;

/**
 * Return the `RedisStore` extending `connect`'s session Store.
 *
 * @param {object} connect
 * @return {Function}
 * @api public
 */

module.exports = function (connect) {

  /**
   * Connect's Store.
   */

  var Store = connect.session.Store;

  /**
   * Initialize TairStore with the given `options`.
   *
   * @param {Object} options
   * @api public
   */

  function TairStore(options) {
    var self = this;

    options = options || {};
    Store.call(this, options);
    this.prefix = options.prefix || 'sess:';

    this.client = options.client;
    this.ttl =  options.ttl;

    self.client.on('error', function () { 
      self.emit('disconnect');
    });
    self.client.on('connect', function () {
      self.emit('connect');
    });
  }

  /**
   * Inherit from `Store`.
   */

  util.inherits(TairStore, Store);

  /**
   * Attempt to fetch session by the given `sid`.
   *
   * @param {String} sid
   * @param {Function} fn
   * @api public
   */

  TairStore.prototype.get = function (sid, fn) {
    sid = this.prefix + sid;
    debug('GET %j', sid);
    this.client.get(sid, function (err, data) {
      if (err) {
        return fn(err);
      }
      if (!data) {
        debug('GOT %j: null', sid);
        return fn();
      }
      var result;
      data = data.toString();
      debug('GOT %j: %j', sid, data);
      try {
        result = JSON.parse(data); 
      } catch (err) {
        return fn(err);
      }
      return fn(null, result);
    });
  };

  /**
   * Commit the given `sess` object associated with the given `sid`.
   *
   * @param {String} sid
   * @param {Session} sess
   * @param {Function} fn
   * @api public
   */

  TairStore.prototype.set = function (sid, sess, fn) {
    sid = this.prefix + sid;
    var maxAge = sess.cookie && sess.cookie.maxAge;
    var ttl = this.ttl || ('number' === typeof maxAge ? maxAge / 1000 | 0 : oneDay);
    try {
      sess = JSON.stringify(sess);
    } catch (err) {
      fn && fn(err);
      return;
    }

    debug('SETEX %j ttl:%s %j', sid, ttl, sess);
    this.client.put(sid, sess, {expired: ttl}, function (err) {
      debug('put complete, error: %s', err);
      fn && fn.apply(this, arguments);
    });
  };

  /**
   * Destroy the session associated with the given `sid`.
   *
   * @param {String} sid
   * @api public
   */

  TairStore.prototype.destroy = function (sid, fn) {
    sid = this.prefix + sid;
    this.client.invalid(sid, fn);
  };

  return TairStore;
};
