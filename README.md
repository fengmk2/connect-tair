connect-tair [![Build Status](https://secure.travis-ci.org/fengmk2/connect-tair.png)](http://travis-ci.org/fengmk2/connect-tair) [![Coverage Status](https://coveralls.io/repos/fengmk2/connect-tair/badge.png)](https://coveralls.io/r/fengmk2/connect-tair) [![Build Status](https://drone.io/github.com/fengmk2/connect-tair/status.png)](https://drone.io/github.com/fengmk2/connect-tair/latest)
=======

![logo](https://raw.github.com/fengmk2/connect-tair/master/logo.png)

[Tair](https://github.com/TBEDP/node-tair) session store for Connect.

`connect-tair` is a Tair session store backed by [tair](https://github.com/TBEDP/node-tair), and is insanely fast :).

## Install

```bash
$ npm install connect-tair
```

## Options

* `client` An existing redis client object you normally get from `new Tair()`
* `ttl` session TTL in seconds, optional, defaulting to `86400` (24 hours)
* `prefix` Key prefix, optional, defaulting to `"sess:"`

## Usage

```js
var connect = require('connect');
var RedisStore = require('connect-tair')(connect);
var Tair = require('tair');

var tair = new Tair({
  dataId: 'market2-daily',
  diamond: 'commonconfig.taobao.net:8080',
  namespace: 171
});

var options = {
  client: tair
};

connect()
  .use(connect.session({ store: new RedisStore(options), secret: 'keyboard cat' }));
```

This means express users may do the following, since `express.session.Store` points to the `connect.session.Store` function:

```js
var express = require('express');
var RedisStore = require('connect-redis')(express);
```

## License 

(The MIT License)

Copyright (c) 2013 fengmk2 &lt;fengmk2@gmail.com&gt;

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
'Software'), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
